import { useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

export default function BarChart({rawData}){
    console.log(rawData)
    const [months, setMonths] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
    const [monthlySales, setMonthlySales] = useState([]);

    useEffect(() => {
        setMonthlySales(months.map(month => {
            let sales = 0;
            rawData.forEach(element => {
                //console.log(moment(element.sale_date).format('MMMM'))
                if(moment(element.sale_data).format('MMMM') === month) {
                    sales += sales + parseInt(element.sales);
                }
            })
            return sales;
        }))

    }, [rawData])
    console.log(monthlySales);
    const data = {
        labels: months,
        datasets: [{
            label: 'Montly Sales for the Year 2020',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: monthlySales
        }]
    }

    return(
        <Bar data={data} />
    )
}