import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import { colorRandomizer } from '../helpers';


export default function PieChart({salesData}){

	console.log(salesData)

    //States to store the array of car brands, sales and bgcolors
    const [brands, setBrands] = useState([]);
    const [sales, setSales] = useState([]);
    const [bgColors, setBgColors] = useState([]);

    useEffect(() => {
        if (salesData.length > 0){
            setBrands(salesData.map(element => element.brand));
            setSales(salesData.map(element => element.sales));

            setBgColors(salesData.map(() => `#${colorRandomizer()}`));
        }
    }, [salesData])
    console.log(brands);
    console.log(sales);
    console.log(bgColors);

    const data = {
        labels: brands,
        datasets: [{
            data: sales,
            backgroundColor:bgColors,
            hoverBackgroundColor: bgColors
        }]
    }
	return (
            <Pie data={data} />

		)

}